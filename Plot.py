import numpy as np
import matplotlib.pyplot as plt
L = 2.0
N = 200
ite = 5

mass_l_error = np.array([1.41477163e-05, 2.29508332e-06, 7.30956572e-07, 1.58061799e-07, 3.72092395e-08])
mass_r_error = np.array([1.14211115e-06, 9.62361290e-07,  1.78777572e-07, 6.35964303e-09, 2.85437196e-09])
energy_error = np.array([1.19994508e-05, 4.71856187e-06, 2.78504271e-07, 4.39092096e-08, 2.92640758e-09])

axis_dx = L/N * np.array([2**(-i) for i in range(ite)])
A = np.vstack([np.log(axis_dx), np.ones(len(axis_dx))]).T

plt.figure(figsize=(5,4))
plt.loglog(axis_dx, mass_l_error,'bo-' ,label = "Left fluid mass rel. error")
m,c = np.linalg.lstsq(A, np.log(mass_l_error))[0]
t = plt.text(axis_dx[-1]*1.3, mass_l_error[-1], str(m)[0:4])
t.set_bbox(dict(facecolor='b', alpha=0.3, edgecolor=None))

plt.loglog(axis_dx, mass_r_error, "o-" , color="grey", label = "Right fluid mass rel. error")
m,c = np.linalg.lstsq(A, np.log(mass_r_error))[0]
t = plt.text(axis_dx[-1]*1.3, mass_r_error[-1], str(m)[0:4])
t.set_bbox(dict(facecolor='grey', alpha=0.3, edgecolor=None))



plt.loglog(axis_dx, energy_error,'g*-', label = "Energy rel. error")
m,c = np.linalg.lstsq(A, np.log(energy_error))[0]
t = plt.text(axis_dx[-1], energy_error[-1]*4.0, str(m)[0:4])
t.set_bbox(dict(facecolor='g', alpha=0.3, edgecolor=None))


plt.loglog(axis_dx, axis_dx**2,'r--',  label = "Ref. 2nd order error")

plt.grid(True)
plt.xlabel('h')
plt.ylabel('Relative errors')
plt.legend(loc='upper left')
plt.grid("on")
plt.xlim([axis_dx[ite - 1]/3.0, axis_dx[0]*3.0])
plt.tight_layout()

plt.savefig("MultiphaseError1D.pdf")
plt.close()

