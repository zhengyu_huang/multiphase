__author__ = 'daniel'


from Space_integrator import *
from copy import deepcopy










def Fluid_Time_Advance(fluid, time, dt, problem_type):

    gamma = fluid.gamma

    dx = fluid.dx

    Interpolation_check = np.array([dx, INTERPOLATION_TOL])

    xx = fluid.xx

    ############################################
    # Update the fluid state variables
    #####################################################


    implicit_or_explicit = problem_type[1]


    if(implicit_or_explicit == 'explicit'):


        ww = fluid.ww_n

        x = fluid.level_set.qq_n[0]

        ###############################################
        #first step k1 = F(w_{n-1}, x_{n-1})
        #                v(w_{n-1}, x_{n-1})
        ###############################################
        k1 = Compute_Rhs_Muscl(ww,x, fluid)

        ww_tmp = ww - dt*k1[0]

        x_tmp = x + dt*k1[1]


        ########################################
        #Phase change upadate
        ############################################
        FS_id = Surrogate_Interface_Id(fluid,x_tmp)


        FS_id_old = Surrogate_Interface_Id(fluid,fluid.level_set.qq_n[0])

        ww_tmp = Phase_Change_Update(ww_tmp, xx,x_tmp, FS_id, FS_id_old, gamma, Interpolation_check)





        ######################################################
        #second step k2 = F(w_{n-1} - dt*k1(0), x_{n-1} + dt*k1(1))
        #second step      v(w_{n-1} - dt*k1(0), x_{n-1} + dt*k1(1))
        ################################################
        k2 = Compute_Rhs_Muscl(ww_tmp, x_tmp, fluid)

        ww_tmp = ww_tmp - dt*k2[0]

        x_tmp = x_tmp + dt*k2[1]

        ww = 0.5*(ww_tmp + ww)

        x = 0.5*(x_tmp + x)


        ##################################################
        # Phase change upadate
        ##################################################

        FS_id = Surrogate_Interface_Id(fluid,x)


        FS_id_old = Surrogate_Interface_Id(fluid,fluid.level_set.qq_n[0])


        ww = Phase_Change_Update(ww, xx,x, FS_id, FS_id_old, gamma, Interpolation_check)


        ######################################################
        #Save the state variables to fluid class
        ######################################################

        fluid.level_set.qq_old_n = deepcopy(fluid.level_set.qq_n)

        fluid.level_set.qq_n[0] = x


        fluid.ww_old_n = fluid.ww_n

        fluid.ww_n = ww

        fluid.t = fluid.t + dt







def Max_Wave_Speed(structure, fluid, eos):

    [gamma] = eos

    v_max = 0

    [FS_id_old_n,FS_id_n] = structure.FS_id_cal( )

    FS_id = FS_id_n

    ww = fluid.ww_n

    uu = np.zeros([3,fluid.n])

    for i in range(0,int(FS_id)):

        uu[:,i] = Conser_To_Pri(ww[:,i],[gamma])

    for i in range(int(FS_id)):

        uu[:,i] = Conser_To_Pri(ww[:,i],[gamma])
        v = abs(uu[1,i]) + np.sqrt(gamma*uu[2,i]/uu[0,i])

        v_max = max(v,v_max)

    return v_max

