__author__ = 'daniel'

import matplotlib.pyplot as plt
from FVM_utility import *
from multiphase import *


##############################################
#Here is the shock tube exact solution
#shock tube [0,L]
###############################################
def f(x, p_l, p_r, gamma, v_l, v_r, c_r, c_l):
    return x*(1 + (gamma - 1)*(v_l - v_r - c_r*(x - 1)/(gamma * \
        np.sqrt((gamma + 1)*(x - 1)/(2*gamma) + 1)))/(2*c_l))**(-2*gamma/(gamma - 1)) - p_l/p_r;

def Exact_soln_point(fluid_info, time,points, L):

    [gamma_l, rho_l, v_l, p_l, gamma_r, rho_r, v_r, p_r] = fluid_info;

    gamma= gamma_l

    c_l = np.sqrt(gamma * p_l/ rho_l)
    c_r = np.sqrt(gamma * p_r/ rho_r)
    arg = (p_l, p_r, gamma, v_l, v_r, c_r, c_l)
    root = optimize.fsolve(f,10,arg)

    p_2 = p_r * root[0]
    v_2 = v_r + c_r * (root[0] - 1)/(gamma * np.sqrt((root[0] - 1)* (gamma + 1) /(2*gamma) + 1))
    s = v_r + c_r * np.sqrt((root[0] - 1)* (gamma + 1) /(2*gamma) + 1)

    c_2 = np.sqrt( ((gamma + 1)/(gamma - 1) + p_2/p_r)/(1 + (gamma + 1)/(gamma - 1) * p_2/p_r) * p_2/p_r * c_r*c_r)
    rho_2 = gamma *p_2/(c_2 * c_2)
    v_3 = v_2
    c_3 = (v_l - v_3) * (gamma - 1)/2 + c_l;
    p_3 = p_2
    rho_3 = gamma *p_3/(c_3*c_3)

    dx = L/ points

    u = np.zeros([3,points])

    x = np.arange(points) * dx + dx/2 - L/2.0


    for i in range(int(points)):


        if(x[i] < time*(v_l - c_l)):
            u[0,i] = rho_l
            u[1,i] = v_l
            u[2,i] = p_l



        elif(x[i] < time*(v_3 - c_3)):
            v_fan = 2/(gamma + 1) *(x[i]/time + (gamma-1)*v_l/2 + c_l)
            c_fan = 2/(gamma + 1) *(x[i]/time + (gamma-1)*v_l/2 + c_l) - x[i]/time
            p_fan = p_l*(c_fan/c_l)**(2*gamma/(gamma -  1))
            rho_fan = gamma*p_fan/c_fan**2
            u[0,i] = rho_fan
            u[1,i] = v_fan
            u[2,i] = p_fan


        elif(x[i] < time*v_2):
            u[0,i] = rho_3
            u[1,i] = v_3
            u[2,i] = p_3

        elif(x[i] < time*s):
            u[0,i] = rho_2
            u[1,i] = v_2
            u[2,i] = p_2


        elif(x[i] > time*s):
            u[0,i] = rho_r
            u[1,i] = v_r
            u[2,i] = p_r

        else:
            print("error")

    x = x + L/2.0

    plt.figure(1)
    plt.plot(x,u[0,:],label = "Exact solution");
    plt.legend(loc='upper right')
    plt.xlabel('x')
    plt.ylabel('density')

    plt.figure(2)
    plt.plot(x,u[1,:],label = "Exact solution");
    plt.legend(loc='upper right')
    plt.xlabel('x')
    plt.ylabel('velocity')



    plt.figure(3)
    plt.plot(x,u[2,:],label = "Exact solution");
    plt.legend(loc='upper right')
    plt.xlabel('x')
    plt.ylabel('pressure')
    return u



def Result(fluid_info,time_info, problem_type, mesh,Exactsolution = False):

    [L,N,CFL, thickness] = mesh

    dx = float(L)/ float(N)

    fluid = Multiphase_1D(mesh, fluid_info, time_info,problem_type)

    gamma_l,gamma_r = fluid.gamma


    xx = fluid.xx

    level_set = fluid.level_set


    ww = fluid.ww_n

    uu = np.zeros([3,N])

    FS_id = Surrogate_Interface_Id(fluid,level_set.qq_n[0])


    for i in range(int(N)):

        if(i < FS_id):

            uu[:,i] = Conser_To_Pri(ww[:,i],gamma_l)

        else:

            uu[:,i] = Conser_To_Pri(ww[:,i],gamma_r)

    plt.figure(figsize=(5,4))
    plt.plot(xx, uu[0,:],'-',label = "Density")
    plt.legend(loc='upper left')
    plt.xlabel('x')
    plt.ylabel('Density')
    plt.tight_layout()

    plt.figure(figsize=(5,4))
    plt.plot(xx, uu[1,:],'-*',label = "Velocity")
    plt.legend(loc='upper right')
    plt.title('Multiphase problem L = %d'%(L))
    plt.tight_layout()

    plt.figure(figsize=(5,4))
    plt.plot(xx, uu[2,:],'-*',label = "Pressure")
    plt.legend(loc='upper right')
    plt.title('Multiphase problem L = %d'%(L))
    plt.tight_layout()

    if(Exactsolution):
        Exact_soln_point(fluid_info,  time_info[0],N , L)


    plt.show()




def Conservation_Values(fluid):

        [x_s,v_s] = fluid.level_set.qq_n

        FS_id = Surrogate_Interface_Id(fluid,x_s)

        dx = fluid.dx

        gamma = fluid.gamma

        ww = fluid.ww_n

        N = fluid.n

        xx = fluid.xx

        ###############################
        # Compute Mass and Energy
        ###############################

        mass = dx*np.sum(ww[0,:])

        mass_l = dx*np.sum(ww[0,0:FS_id])

        mass_r = dx*np.sum(ww[0,FS_id:N])

        energy = dx*np.sum(ww[2,:])


        x_si = FS_id*dx

        #Ghost interpolation

        xx_ll = xx[FS_id - 2]
        ww_ll = ww[:, FS_id - 2]
        xx_l  = xx[FS_id - 1]
        ww_l  = ww[:, FS_id - 1]
        xx_r  = xx[FS_id]
        ww_r  = ww[:, FS_id]
        xx_rr = xx[FS_id + 1]
        ww_rr = ww[:, FS_id + 1]

        w_Rl,w_Rr = SIV(xx_ll,ww_ll,xx_l,ww_l,x_s,xx_r,ww_r,xx_rr,ww_rr,gamma, fluid.Interpolation_check)

        mass_correction = (w_Rl[0] - w_Rr[0])*(x_s - x_si)

        energy_correction =(w_Rl[2] - w_Rr[2])*(x_s - x_si)

        mass = mass + mass_correction
        mass_l = mass_l + w_Rl[0]*(x_s - x_si)
        mass_r = mass_r - w_Rr[0]*(x_s - x_si)
        energy = energy + energy_correction

        print("xs is ", x_s, " mass is ", mass," energy is ", energy)

        return mass, energy,mass_l,mass_r


def Conservation_error(fluid_info,time_info, test_info, problem_type, mesh):
    Time = time_info[0]

    [N, ite] = test_info

    energy_error = np.zeros(ite)

    mass_error = np.zeros(ite)

    mass_l_error = np.zeros(ite)

    mass_r_error = np.zeros(ite)

    axis_dx = np.zeros(ite)

    O2_check = np.zeros(ite)


    for i in range(ite):

        mesh[1]  = N

        eos = [fluid_info[0]]

        fluid = Multiphase_1D(mesh, fluid_info, time_info,problem_type)

        dx = fluid.dx

        axis_dx[i] = dx

        O2_check[i] =0.10/N**SIV_ORDER

        real_mass = fluid.real_mass



        real_energy = fluid.real_energy

        ###############################
        # Compute Energy
        ###############################
        mass,energy,mass_l, mass_r  = Conservation_Values(fluid)

        mass_error[i] = abs(mass - real_mass)/real_mass

        energy_error[i] = abs(energy - real_energy)/real_energy

        mass_l_error[i] = abs(mass_l - fluid.real_mass_l)/fluid.real_mass_l

        mass_r_error[i] = abs(mass_r - fluid.real_mass_r)/fluid.real_mass_r

        N = 2*N

    print("total mass error is", mass_error)

    print("total energy error is", energy_error)

    print("left mass error is", mass_l_error)

    print("right mass error is", mass_r_error)




    plt.figure(1)
    plt.loglog(axis_dx, mass_error,'ro-' , label = "mass error")
    plt.loglog(axis_dx, O2_check,'k--', label = "O(h^%d)"%(SIV_ORDER))
    plt.grid(True)
    plt.xlabel('dx')
    plt.ylabel('mass_error')
    plt.legend(loc='upper right')
    plt.xlim([axis_dx[0]*3.0, axis_dx[ite - 1]/3.0])
    plt.title('Mass error of flexible motion L = %d'%(fluid.L))

    plt.figure(2)
    plt.loglog(axis_dx, energy_error,'ro-' , label = "energy error")
    plt.loglog(axis_dx, O2_check,'k--',   label = "O(h^%d)"%(SIV_ORDER))
    plt.grid(True)
    plt.xlabel('dx')
    plt.ylabel('energy_error')
    plt.legend(loc='upper right')
    plt.xlim([axis_dx[0]*3.0, axis_dx[ite - 1]/3.0])

    plt.title('Energy Error of flexible motion L = %d'%(fluid.L))


    plt.show()

