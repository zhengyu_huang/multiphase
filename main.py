__author__ = 'daniel'
#################################
#The body motion parameters can be set in body_motion.py
#Geometry, error check parameters can be set in global_variable.py
#Test, fluid parameter could be set here
#The initial condition could be set in FIVER.py, we have constant and linear initialization
######################################

from ErrorCheck import *
Time = 0.25
#Time = 0.5
N = 200
L = 2.0
thickness = 1.0e-12
CFL = 0.1
ite = 5

#TODO we assume that Time divides by dt


#arg[0] is gamma_l
#arg[1:4] is desity rho_l, velocity v_l and pressure p_l
#arg[0] is gamma_r
#arg[1:4] is desity rho_r, velocity v_r and pressure p_r
#in FIVER, there is an initalization function
#to initialize the fluid state,
#fluid_info = [gamma_l, rho_l, v_l, p_l, gamma_r, rho_r, v_r, p_r]
fluid_info = np.array([1.4, 1.0, 0.0, 1.0, 1.4, 0.125, 0.0, 0.1])

#End time is arg[0] Time, arg[1] is the parameter about
#CFL number, if arg[1] == 0, CFL is const, dt =CFL*dx
time_info = np.array([Time, 0])


#arg[0]: tube length
#arg[1]: number of cells
#CFL number, actually it is the ratio of dt/dx
mesh = [L,N,CFL, thickness]

#arg[0]: method A6 or A7
#arg[1]: implicit or explicit
#arg[2]: forced or aero
problem_type = ['A6','explicit']



#arg[0] is cell number N
#arg[1] is iteration time, in Error,
#we compute N, 2N ...
test_info = np.array([N,ite])


Result(fluid_info,time_info, problem_type, mesh)

Conservation_error(fluid_info, time_info,test_info, problem_type, mesh)




