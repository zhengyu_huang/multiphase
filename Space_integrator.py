__author__ = 'daniel'

from FVM_utility import *

def Phase_Change_Update(ww,xx,x_s,FS_id_n,FS_id_old_n,gamma,Interpolation_check):
    #if(FS_id_n == FS_id_old_n):
    #    return ww
    #print 'Phase change here ##################################'

    [gamma_l, gamma_r] = gamma
    if(FS_id_n == FS_id_old_n + 1):

        w_Rl,w_Rr = SIV(xx[FS_id_old_n - 2], ww[:, FS_id_old_n - 2], xx[FS_id_old_n - 1], ww[:,FS_id_old_n - 1], x_s,
                        xx[FS_id_n], ww[:, FS_id_n], xx[FS_id_n + 1], ww[:,FS_id_n + 1],gamma, Interpolation_check)

        ww[:,FS_id_old_n] = Interpolation(ww[:,FS_id_old_n - 1],xx[FS_id_old_n - 1], w_Rl, x_s, xx[FS_id_n - 1], gamma_l,Interpolation_check)

    elif(FS_id_n == FS_id_old_n - 1):

        w_Rl,w_Rr = SIV(xx[FS_id_n - 2], ww[:, FS_id_n - 2], xx[FS_id_n - 1], ww[:,FS_id_n - 1], x_s,
                        xx[FS_id_old_n], ww[:, FS_id_old_n], xx[FS_id_old_n + 1], ww[:,FS_id_old_n + 1],gamma, Interpolation_check)

        ww[:,FS_id_n] = Interpolation(ww[:,FS_id_old_n ],xx[FS_id_old_n], w_Rr, x_s, xx[FS_id_n],gamma_r,  Interpolation_check)


    return ww





def Compute_Rhs_Muscl(ww,x, fluid):

    gamma = fluid.gamma

    [gamma_l,gamma_r] = gamma

    eps = 10**(-10)

    N = fluid.n;

    flux = np.zeros([3, N + 1])

    rhs  = np.zeros([3, N])

    dx = fluid.dx   #assume all the cells have the same size

    xx = fluid.xx

    Interpolation_check = np.array([dx, INTERPOLATION_TOL])

    dt = fluid.dt


    FS_id = Surrogate_Interface_Id(fluid,x)









    ##########################################################################
    # Compute exact Riemann problem on both sides of the fluid/fluid interface
    ##########################################################################

    xx_ll = xx[FS_id - 2]
    ww_ll = ww[:, FS_id - 2]
    xx_l  = xx[FS_id - 1]
    ww_l  = ww[:, FS_id - 1]
    xx_r  = xx[FS_id]
    ww_r  = ww[:, FS_id]
    xx_rr = xx[FS_id + 1]
    ww_rr = ww[:, FS_id + 1]


    w_Rl,w_Rr = SIV(xx_ll,ww_ll,xx_l,ww_l,x,xx_r,ww_r,xx_rr,ww_rr,gamma, fluid.Interpolation_check)

    v = w_Rl[1]/w_Rl[0]


    #################################
    #Save the interface velocity
    ##################################



    ######################################################
    #compute numerical flux
    #FS_id is the number of the edge which intersects the structure surface
    ######################################################

    for i in range(int(N) + 1):


        #################################################
        # Left State
        #######################################################
        #TODO wall boundary implementation issue
        if(i == 0):
            ww_ll = 3.0*ww[:,0] - 2.0*ww[:,1]
            ww_l  = 2.0*ww[:,0] -     ww[:,1]

            xx_ll = xx[0] - 2.0*dx
            xx_l  = xx[0] -     dx

        elif(i == 1):

            xx_ll = xx[0] - dx
            xx_l  = xx[0]

            ww_l  = ww[:,0]

            ww_ll = np.array([ww[0,0],-ww[1,0],ww[2,0]])
        else:

            xx_ll = xx[i - 2]
            xx_l  = xx[i - 1]

            ww_ll = ww[:,i - 2]
            ww_l  = ww[:,i - 1]


        ########################################
        # Right State
        ##############################################################

        if(i == N ):
            ww_rr = 3.0*ww[:,N - 1] - 2.0*ww[:,N - 2]
            ww_r  = 2.0*ww[:,N - 1] -     ww[:,N - 2]

            xx_rr = xx[N - 1] + 2.0*dx
            xx_r  = xx[N - 1] + 1.0*dx
        elif(i == N - 1):
            ww_rr = 2.0*ww[:,i] - ww[:,i - 1]
            ww_r  = ww[:,i]

            xx_rr = xx[i] + dx
            xx_r  = xx[i]
        else:
            ww_r  = ww[:,i]
            ww_rr = ww[:,i + 1]

            xx_r  = xx[i]
            xx_rr = xx[i + 1]

        #######################################
        # reconstruction
        #######################################
        if( i < FS_id - 1 ):
            uu_r = Conser_To_Pri(ww_r,gamma_l)
            uu_rr = Conser_To_Pri(ww_rr,gamma_l)
            uu_l = Conser_To_Pri(ww_l,gamma_l)
            uu_ll = Conser_To_Pri(ww_ll,gamma_l)

            L = (uu_r - uu_l)/(uu_l - uu_ll + eps);

            phi = Limiter_Fun(L)

            uu_L_HO = uu_l + 0.5* phi *(uu_l - uu_ll)

            ww_L_HO = Pri_To_Conser(uu_L_HO,gamma_l)

            R = (uu_r - uu_l)/(uu_rr - uu_r + eps);

            phi = Limiter_Fun(R)

            uu_R_HO = uu_r - 0.5* phi *(uu_rr - uu_r)

            ww_R_HO = Pri_To_Conser(uu_R_HO,gamma_l)

            flux[:, i] = FF_Roe_Flux(ww_L_HO, ww_R_HO, gamma_l)

        elif( i > FS_id + 1 ):
            uu_r  = Conser_To_Pri(ww_r,gamma_r)
            uu_rr = Conser_To_Pri(ww_rr,gamma_r)
            uu_l  = Conser_To_Pri(ww_l,gamma_r)
            uu_ll = Conser_To_Pri(ww_ll,gamma_r)

            L = (uu_r - uu_l)/(uu_l - uu_ll + eps);

            phi = Limiter_Fun(L)

            uu_L_HO = uu_l + 0.5* phi *(uu_l - uu_ll)

            ww_L_HO = Pri_To_Conser(uu_L_HO,gamma_r)

            R = (uu_r - uu_l)/(uu_rr - uu_r + eps);

            phi = Limiter_Fun(R)

            uu_R_HO = uu_r - 0.5* phi *(uu_rr - uu_r)

            ww_R_HO = Pri_To_Conser(uu_R_HO,gamma_r)

            flux[:, i] = FF_Roe_Flux(ww_L_HO, ww_R_HO, gamma_r)

        elif(i == FS_id - 1 ):

            uu_r = Conser_To_Pri(ww_r,gamma_l)

            uu_l = Conser_To_Pri(ww_l,gamma_l)

            uu_ll = Conser_To_Pri(ww_ll,gamma_l)

            L = (uu_r - uu_l)/(uu_l - uu_ll + eps);

            phi = Limiter_Fun(L)

            uu_L_HO = uu_l + 0.5* phi *(uu_l - uu_ll)

            ww_L_HO = Pri_To_Conser(uu_L_HO,gamma_l)

            ww_rr_g = Safe_Interpolation(ww_l, xx_l, ww_r, xx_r, w_Rl, x, xx_rr,gamma_l, Interpolation_check)

            uu_rr_g = Conser_To_Pri(ww_rr_g,gamma_l)

            R = (uu_r - uu_l)/(uu_rr_g - uu_r + eps);

            phi = Limiter_Fun(R)

            uu_R_HO = uu_r - 0.5* phi *(uu_rr_g - uu_r)

            ww_R_HO = Pri_To_Conser(uu_R_HO,gamma_l)

            flux[:, i] = FF_Roe_Flux(ww_L_HO, ww_R_HO, gamma_l)

        elif(i == FS_id + 1 ):
            uu_r = Conser_To_Pri(ww_r,gamma_r)

            uu_rr = Conser_To_Pri(ww_rr,gamma_r)

            uu_l = Conser_To_Pri(ww_l,gamma_r)



            ww_ll_g = Safe_Interpolation(ww_r, xx_r, ww_l, xx_l, w_Rr, x, xx_ll,gamma_r, Interpolation_check)

            uu_ll_g = Conser_To_Pri(ww_ll_g,gamma_r)

            L = (uu_r - uu_l)/(uu_l - uu_ll_g + eps);

            phi = Limiter_Fun(L)

            uu_L_HO = uu_l + 0.5* phi *(uu_l - uu_ll_g)

            ww_L_HO = Pri_To_Conser(uu_L_HO,gamma_r)

            R = (uu_r - uu_l)/(uu_rr - uu_r + eps);

            phi = Limiter_Fun(R)

            uu_R_HO = uu_r - 0.5* phi *(uu_rr - uu_r)

            ww_R_HO = Pri_To_Conser(uu_R_HO,gamma_r)

            flux[:, i] = FF_Roe_Flux(ww_L_HO, ww_R_HO, gamma_r)

        else:
            x_si = 0.5*(xx_l + xx_r)

            w_si_l = Safe_Interpolation(ww_ll, xx_ll, ww_l, xx_l, w_Rl, x, x_si,gamma_l, Interpolation_check)

            w_si_r = Safe_Interpolation(ww_rr, xx_rr, ww_r, xx_r, w_Rr, x, x_si,gamma_r, Interpolation_check)

            interface_flux_l = Flux(w_si_l, gamma_l)

            interface_flux_r = Flux(w_si_r, gamma_r)

            diff = interface_flux_r - interface_flux_l





    for i in range(int(N)):
        if(i < FS_id - 1 or i > FS_id):

            rhs[:,i] = (flux[:, i + 1] - flux[:, i])/dx
        elif(i == FS_id - 1):

            rhs[:,i] = (interface_flux_l - flux[:, i])/dx
        elif(i == FS_id):
            rhs[:,i] = (flux[:, i + 1] - interface_flux_r)/dx
        else:
            print('Error here in computing rhs')




    return rhs,v

