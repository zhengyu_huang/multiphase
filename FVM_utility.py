__author__ = 'daniel'

import numpy as np
from scipy import optimize
global SIV_ORDER
global INTERPOLATION_TOL
SIV_ORDER = 2
INTERPOLATION_TOL = 0.1


def Pri_To_Conser(u,gamma):
    [rho, v, p] = u;
    return  np.array([rho, rho*v, rho*v*v/2 + p/(gamma - 1)])


def Conser_To_Pri(u, gamma):
    [w1, w2, w3] = u
    rho = w1;
    v = w2/w1;

    p = (w3 - w2*v/2) * (gamma - 1)
    return np.array([rho, v, p])


def Flux(ww, gamma):
    u = Conser_To_Pri(ww, gamma)
    [rho, v, p] = u
    return np.array([rho*v, rho*v*v +p, (rho*v*v/2 + gamma * p/(gamma - 1))*v])

def Safe_Interpolation(w0,x0,w1,x1,w2,x2,x3,gamma,check):
    [dx,tol] = check
    u0 = Conser_To_Pri(w0, gamma)
    u1 = Conser_To_Pri(w1, gamma)
    u2 = Conser_To_Pri(w2, gamma)

    if(abs(x2 - x1) > tol*dx):
        u3 = (u2 - u1) * (x3 - x2)/(x2 - x1) + u2
        if(u3[0] >= 0 and u3[2] >= 0):
            w3 =Pri_To_Conser(u3,gamma)
            return w3
        else:
            print("w1 is ", w1, " w2 is ", w2)
            print("In Interpolation, encounter negative values ")
    else:
        u3 = (u2 - u0) * (x3 - x2)/(x2 - x0) + u2
        if(u3[0] >= 0 and u3[2] >= 0):
            w3 =Pri_To_Conser(u3,gamma)
            return w3
        else:
            print("w1 is ", w1, " w2 is ", w2)
            print("In Interpolation, encounter negative values ")

    if(abs(x1 - x3) > abs(x2 -x3)):
        return w2
    else:
        return w1


def Interpolation(w1, x1, w2, x2, x3, gamma, check):
    [dx,tol] = check
    u1 = Conser_To_Pri(w1, gamma)
    u2 = Conser_To_Pri(w2, gamma)

    if(abs(x2 - x1) > tol*dx):
        u3 = (u2 - u1) * (x3 - x2)/(x2 - x1) + u2
        if(u3[0] >= 0 and u3[2] >= 0):
            w3 =Pri_To_Conser(u3,gamma)
            return w3
        else:
            print("w1 is ", w1, " w2 is ", w2)
            print("In Interpolation, encounter negative values ")
    else:
        print("In Interpolation, points are too closed")

    if(abs(x1 - x3) > abs(x2 -x3)):
        return w2
    else:
        return w1


#####################################################
# Assume the fluid/fluid or fluid/structure interface is at x
# return the surrogate interface id
#####################################################
def Surrogate_Interface_Id(fluid, x):
    dx = fluid.dx
    thickness = fluid.level_set.thickness
    FS_id = round((x + thickness)/dx)

    return FS_id


#######################################################
# To solve Riemann problem, we need solve algebraic equation
# f(p, W_L, W_R) = f_L(p,W_L) + f_R(p,W_R) + v_R - v_L = 0
#######################################################

def Riemann_f(p, u_l, gamma_l, u_r, gamma_r):

    #print 'evaluate Riemann F, p is ', p

    [rho_l, v_l, p_l] = u_l

    [rho_r, v_r, p_r] = u_r

    f_l = 0;
    f_r = 0;

    if(p > p_l):
        A_l = 2/((gamma_l + 1)*rho_l)

        B_l = (gamma_l - 1)/(gamma_l + 1)*p_l

        f_l = (p - p_l)*np.sqrt(A_l/(p + B_l))

    else:
        c_l = np.sqrt(gamma_l * p_l/ rho_l)

        f_l = 2*c_l/(gamma_l - 1)*((p/p_l) **((gamma_l - 1)/(2*gamma_l)) - 1.0)



    if(p > p_r):
        A_r = 2/((gamma_r + 1)*rho_r)

        B_r = (gamma_r - 1)/(gamma_r + 1)*p_r

        f_r = (p - p_r)*np.sqrt(A_r/(p + B_r))

    else:
        c_r = np.sqrt(gamma_r * p_r/ rho_r)

        f_r = 2*c_r/(gamma_r - 1)*((p/p_r) **((gamma_r - 1)/(2*gamma_r)) - 1.0)

    return f_l + f_r + v_r - v_l





def Riemann_df(p, u_l, gamma_l, u_r, gamma_r):

    [rho_l, v_l, p_l] = u_l

    [rho_r, v_r, p_r] = u_r

    df_l = 0;

    df_r = 0;

    if(p > p_l):
        A_l = 2/((gamma_l + 1)*rho_l)

        B_l = (gamma_l - 1)/(gamma_l + 1)*p_l

        df_l = np.sqrt(A_l/(p + B_l))*(1 - (p - p_l)/(2*(B_l + p)))

    else:
        c_l = np.sqrt(gamma_l * p_l/ rho_l)

        df_l = (p/p_l) **(-(gamma_l + 1)/(2*gamma_l))/(rho_l*c_l)


    if(p > p_r):
        A_r = 2/((gamma_r + 1)*rho_r)

        B_r = (gamma_r - 1)/(gamma_r + 1)*p_r

        df_r = np.sqrt(A_r/(p + B_r))*(1 - (p - p_r)/(2*(B_r + p)))

    else:
        c_r = np.sqrt(gamma_r * p_r/ rho_r)

        df_r = (p/p_r) **(-(gamma_r + 1)/(2*gamma_r))/(rho_r*c_r)




    return df_l + df_r

def PressureGuess(u_l,gamma_l, u_r, gamma_r):
    p_tol = 1.0e-4
    [rho_l, v_l, p_l] = u_l

    [rho_r, v_r, p_r] = u_r

    c_l = np.sqrt(gamma_l * p_l/ rho_l)
    c_r = np.sqrt(gamma_r * p_r/ rho_r)


    p_pv = 0.5*(p_l + p_r) - 0.125*(v_r - v_l)*(rho_l + rho_r)*(c_l + c_r)
    p_pv = max(p_tol,p_pv)

    return p_pv


    return (p_l + p_r)/2.0

def Newton_Solver(func, x0, args=(), fprime = None):
    tol = 1.0e-8
    IteMax = 100
    p_tol = 1e-4
    x_old = x0
    Ite = 0;
    while(Ite < IteMax):
        f_old = func(x_old,*args)
        x_new = x_old - f_old/fprime(x_old,*args)

        if(x_new <= 0):
            x_new = p_tol;
        f_new = func(x_new,*args)


        if(abs(f_new + f_old)< tol or abs((f_new - f_old)/(f_new + f_old))< tol ):
            break;
        else:
            x_old = x_new
    if(Ite == 100):
        print('Newton method diverges')
    return x_new






def FFRiemann(w_l, gamma_l, w_r, gamma_r):
    u_l = Conser_To_Pri(w_l,gamma_l)
    u_r = Conser_To_Pri(w_r,gamma_r)

    [rho_l, v_l, p_l] = u_l;

    [rho_r, v_r, p_r] = u_r;


    arg = (u_l, gamma_l, u_r, gamma_r)

    p_0 = PressureGuess(u_l,gamma_l, u_r, gamma_r)

    #root = optimize.fsolve(Riemann_f,p_0, arg, Riemann_df)

    #p_I = root[0]
    p_I =  Newton_Solver(Riemann_f,p_0, arg, Riemann_df)

    f_l = 0;

    f_r = 0;

    rho_Il = 0;

    rho_Ir = 0;

    if(p_I > p_l): #shock
        A_l = 2/((gamma_l + 1)*rho_l)

        B_l = (gamma_l - 1)/(gamma_l + 1)*p_l

        f_l = (p_I - p_l)*np.sqrt(A_l/(p_I + B_l))

        rho_Il = rho_l*(p_I/p_l + (gamma_l - 1)/(gamma_l + 1))/((gamma_l - 1)/(gamma_l + 1)*p_I/p_l + 1)

    else: #rarefaction
        c_l = np.sqrt(gamma_l * p_l/ rho_l)

        f_l = 2*c_l/(gamma_l - 1)*((p_I/p_l) **((gamma_l - 1)/(2*gamma_l)) - 1.0)

        rho_Il = rho_l*(p_I/p_l)**(1/gamma_l)


    if(p_I > p_r): #shock
        A_r = 2/((gamma_r + 1)*rho_r)

        B_r = (gamma_r - 1)/(gamma_r + 1)*p_r

        f_r = (p_I - p_r)*np.sqrt(A_r/(p_I + B_r))

        rho_Ir = rho_r*(p_I/p_r + (gamma_r - 1)/(gamma_r + 1))/((gamma_r - 1)/(gamma_r + 1)*p_I/p_r + 1)

    else: #rarefaction
        c_r = np.sqrt(gamma_r * p_r/ rho_r)

        f_r = 2*c_r/(gamma_r - 1)*((p_I/p_r) **((gamma_r - 1)/(2*gamma_r)) - 1.0)

        rho_Ir = rho_r*(p_I/p_r)**(1/gamma_r)

    v_I = v_l - f_l

    u_Il = np.array([rho_Il, v_I, p_I])

    u_Ir = np.array([rho_Ir, v_I, p_I])

    w_Rl = Pri_To_Conser(u_Il,gamma_l)

    w_Rr = Pri_To_Conser(u_Ir,gamma_r)

    return w_Rl,w_Rr

#########################################################
# Validation test for Riemann solver
########################################################
def Riemann_Solver_Test():






    print('Validation test for Riemann solver')
    gamma_l = 1.4
    gamma_r = 1.4
    u_l = np.array([1.0,0.0,1.0])
    u_r = np.array([0.125,0.0,0.1])
    w_l = Pri_To_Conser(u_l,gamma_l)
    w_r = Pri_To_Conser(u_r,gamma_r)
    w_Rl,w_Rr = FFRiemann(w_l, gamma_l, w_r, gamma_r)
    u_Rl = Conser_To_Pri(w_Rl,gamma_l)
    u_Rr = Conser_To_Pri(w_Rr,gamma_r)
    print('Test 1: ', Riemann_f(0.30313,u_l,gamma_l,u_r,gamma_r))

    print(u_Rl, u_Rr)

    gamma_l = 1.4
    gamma_r = 1.4
    u_l = np.array([1.0, -2.0, 0.4])
    u_r = np.array([1.0,  2.0, 0.4])
    w_l = Pri_To_Conser(u_l,gamma_l)
    w_r = Pri_To_Conser(u_r,gamma_r)
    w_Rl,w_Rr = FFRiemann(w_l, gamma_l, w_r, gamma_r)
    u_Rl = Conser_To_Pri(w_Rl,gamma_l)
    u_Rr = Conser_To_Pri(w_Rr,gamma_r)

    print('Test 2: ',Riemann_f(0.00189,u_l,gamma_l,u_r,gamma_r))

    print(u_Rl, u_Rr)

    u_l = np.array([1.0, 0.0, 1000.0])
    u_r = np.array([1.0, 0.0, 0.01])
    w_l = Pri_To_Conser(u_l,gamma_l)
    w_r = Pri_To_Conser(u_r,gamma_r)
    w_Rl,w_Rr = FFRiemann(w_l, gamma_l, w_r, gamma_r)
    u_Rl = Conser_To_Pri(w_Rl,gamma_l)
    u_Rr = Conser_To_Pri(w_Rr,gamma_r)
    print('Test 3: ',Riemann_f(460.894,u_l,gamma_l,u_r,gamma_r))

    print(u_Rl, u_Rr)

    u_l = np.array([1.0, 0.0, 0.01])
    u_r = np.array([1.0, 0.0, 100.0])
    w_l = Pri_To_Conser(u_l,gamma_l)
    w_r = Pri_To_Conser(u_r,gamma_r)
    w_Rl,w_Rr = FFRiemann(w_l, gamma_l, w_r, gamma_r)
    u_Rl = Conser_To_Pri(w_Rl,gamma_l)
    u_Rr = Conser_To_Pri(w_Rr,gamma_r)
    print('Test 4: ',Riemann_f(46.0950,u_l,gamma_l,u_r,gamma_r))

    print(u_Rl, u_Rr)


    u_l = np.array([5.99924,  19.5975,   460.894])
    u_r = np.array([5.992421, -6.19633,   46.0950])
    w_l = Pri_To_Conser(u_l,gamma_l)
    w_r = Pri_To_Conser(u_r,gamma_r)
    w_Rl,w_Rr = FFRiemann(w_l, gamma_l, w_r, gamma_r)
    u_Rl = Conser_To_Pri(w_Rl,gamma_l)
    u_Rr = Conser_To_Pri(w_Rr,gamma_r)
    print('Test 5: ',Riemann_f(1691.64,u_l,gamma_l,u_r,gamma_r))

    print(u_Rl, u_Rr)


















def SIV(x_ll, w_ll, x_l, w_l, x_s, x_r, w_r, x_rr, w_rr, gamma, Interpolation_check):
    [dx, tol] = Interpolation_check

    [gamma_l,gamma_r] = gamma

    Order = 2
    ##########
    #1 order
    ########################
    if(Order == 1):
        w_Rl, w_Rr = FFRiemann(w_l, gamma_l, w_r, gamma_r)

    ###########
    #2 order
    ###########################
    else:
        w_l_recons = Interpolation(w_ll, x_ll, w_l, x_l, x_s, gamma_l, Interpolation_check)

        w_r_recons = Interpolation(w_rr, x_rr, w_r, x_r, x_s, gamma_r, Interpolation_check)
        #ww_si = Interpolation(ww1, x1, ww_bs, x_wall, x_si,eos, Interpolation_check)

        w_Rl, w_Rr = FFRiemann(w_l_recons,gamma_l, w_r_recons, gamma_r)

    return w_Rl, w_Rr










def Limiter_Fun(r):
    f =np.array([0.,0.,0.])
    for i in range(3):
        if(r[i] > 0):
            f[i] = 2*r[i]/(r[i] + 1)
        else:
            f[i] = 0
    return f



# this is Fluid Fluid Roe Flux function
def FF_Roe_Flux(ww_l, ww_r, gamma):
    u_l = Conser_To_Pri(ww_l,gamma)
    u_r = Conser_To_Pri(ww_r,gamma)
    [rho_l, v_l, p_l] = u_l;
    [rho_r, v_r, p_r] = u_r;

    c_l = np.sqrt(gamma * p_l/ rho_l)
    c_r = np.sqrt(gamma * p_r/ rho_r)
    w_l = np.array([rho_l, rho_l*v_l, rho_l*v_l*v_l/2 + p_l/(gamma - 1)])
    w_r = np.array([rho_r, rho_r*v_r, rho_r*v_r*v_r/2 + p_r/(gamma - 1)])
    h_l = v_l*v_l/2.0 + gamma * p_l/(rho_l * (gamma - 1));
    h_r = v_r*v_r/2.0 + gamma * p_r/(rho_r * (gamma - 1));

    # compute the Roe-averaged quatities
    v_rl = (np.sqrt(rho_l)*v_l + np.sqrt(rho_r)*v_r)/ (np.sqrt(rho_r) + np.sqrt(rho_l));
    h_rl = (np.sqrt(rho_l)*h_l + np.sqrt(rho_r)*h_r)/ (np.sqrt(rho_r) + np.sqrt(rho_l));
    c_rl = np.sqrt((gamma - 1)*(h_rl - v_rl * v_rl/2));
    rho_rl = np.sqrt(rho_r * rho_l);


    du = v_r - v_l
    dp = p_r - p_l
    drho = rho_r - rho_l
    du1 = du + dp/(rho_rl * c_rl);
    du2 = drho - dp/(c_rl * c_rl);
    du3 = du - dp/(rho_rl * c_rl);

    #compute the Roe-average wave speeds
    lambda_1 = v_rl + c_rl;
    lambda_2 = v_rl;
    lambda_3 = v_rl - c_rl;

    #compute the right characteristic vectors
    r_1 =  rho_rl/(2 * c_rl) * np.array([1 ,v_rl + c_rl, h_rl + c_rl * v_rl]);
    r_2 = np.array([1, v_rl, v_rl * v_rl/2]);
    r_3 = -rho_rl/(2 * c_rl) * np.array([1,v_rl - c_rl, h_rl - c_rl * v_rl]);


    #compute the fluxes
    mach = v_rl/ c_rl
    if(mach <=-1):
        flux = np.array([rho_r*v_r, rho_r*v_r*v_r + p_r, rho_r*v_r*h_r])
    elif(mach <= 0 and mach >= -1):
        flux = np.array([rho_r * v_r, rho_r*v_r**2 + p_r,rho_r*h_r*v_r]) - r_1*lambda_1*du1;
    elif(mach >= 0 and mach <= 1):
        flux = np.array([rho_l * v_l, rho_l*v_l**2 + p_l,rho_l*h_l*v_l]) + r_3*lambda_3*du3;
    else:
        flux = np.array([rho_l * v_l, rho_l*v_l**2 + p_l,rho_l*h_l*v_l])

    return flux




################################################
# This is two-phase fluid class
###########################################################

class Fluid:
    def __init__(self, space_info):

        [L, N, CFL, thickness] = space_info

        dx = float(L)/ float(N)

        self.dx = dx

        self.n = N

        self.L = L

        self.t = 0

        self.dt = dx*CFL

        self.xx = np.arange(dx/2, L, dx)

        self.ww_n = np.zeros([3,N])

        self.ww_old_n = np.zeros([3,N])

        self.level_set = LevelSet(L/2.0, thickness)

        self.Interpolation_check = np.array([dx, INTERPOLATION_TOL])

        self.real_mass = 0.0


        self.real_energy = 0.0

    def ShockTube_Initial(self,fluid_info):

        [gamma_l, rho_l, v_l, p_l, gamma_r, rho_r, v_r, p_r] = fluid_info

        fluid_l = np.array([rho_l, v_l, p_l]);

        fluid_r = np.array([rho_r, v_r, p_r]);

        self.gamma = np.array([gamma_l, gamma_r])

        for i in range(int(self.n)):

            if(self.xx[i] <= self.L/2.0):

                uu_l = Pri_To_Conser(fluid_l, gamma_l)

                self.ww_n[:,i]  = uu_l

                self.ww_old_n[:,i] = uu_l
            else:
                uu_r = Pri_To_Conser(fluid_r, gamma_r)

                self.ww_n[:,i]  = uu_r

                self.ww_old_n[:,i] = uu_r


        self.real_mass = (rho_l + rho_r)*self.L/2



        self.real_energy  = (0.5*rho_l*v_l*v_l + p_l/(gamma_l -1) + 0.5*rho_r*v_r*v_r + p_r/(gamma_r -1))*self.L/2

    #This is smooth initialization, as Alex Main's Thesis

    def Smooth_Initial(self):
        L = self.L

        xx = self.xx

        dx = self.dx

        gamma_l, rho_l, v_l, gamma_r, rho_r, v_r = 4.4 , 1.0, 0.0, 1.4, 1.0, 0.0

        self.gamma = np.array([gamma_l, gamma_r])

        #level set initial position
        x_0 = L/2

        for i in range(int(self.n)):

            if(xx[i] <= x_0):

                if(xx[i] >= L/2.0 - 0.2):
                    p_l = 1.0e6*(L/2.0 - 0.2 -xx[i])**4 *(L/2.0 + 0.2 - xx[i])**4 + 1.0
                else:
                    p_l = 1.0

                fluid_l = np.array([rho_l, v_l, p_l])

                ww_l = Pri_To_Conser(fluid_l, gamma_l)

                self.ww_n[:,i]  = ww_l

                self.ww_old_n[:,i] = ww_l
            else:
                if(xx[i] <= L/2.0 + 0.2):

                    p_r = 1.0e6*(L/2.0 - 0.2 -xx[i])**4 *(L/2.0 + 0.2 - xx[i])**4 + 1.0
                else:
                    p_r  = 1.0

                fluid_r = np.array([rho_r, v_r, p_r])

                ww_r = Pri_To_Conser(fluid_r, gamma_r)

                self.ww_n[:,i]  = ww_r

                self.ww_old_n[:,i] = ww_r

            self.real_energy = self.real_energy + self.ww_n[2,i]*dx


        self.real_mass = (rho_l + rho_r)*self.L/2

        self.real_mass_l = rho_l*self.L/2.0

        self.real_mass_r = rho_r*self.L/2.0


        ########change level set position

        self.level_set.qq_n[0] = x_0
        self.level_set.qq_old_n[0] = x_0














class LevelSet:
    def __init__(self, x, thickness):

        self.qq_n = np.array([x, 0.0])
        self.qq_old_n = np.array([x, 0.0])

        self.thickness = thickness









    def LevelSetReinitialize(self):
        print ('TODO')



