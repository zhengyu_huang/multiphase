__author__ = 'daniel'



from Time_integrator import *

# L is the length of the tube, assuming that the tube is at [0, L]
# N is the number of fluid ceils
# structure_inf = [xs_0, vs_0, ks, ms], which are parameters for structure
# fluid_inf =[gamma,rho_l, v_l, p_l], which are parameters for the fluid
# mesh  = [L, N ,CFL]








def Multiphase_1D(mesh, fluid_info, time_info, problem_type):
    print('Start Multiphase solver')

    [L,N,CFL, thickness] = mesh;

    [gamma_l, rho_l, v_l, p_l, gamma_r, rho_r, v_r, p_r] = fluid_info

    [time, time_type] = time_info


    fluid = Fluid(mesh)

    #fluid.ShockTube_Initial(fluid_info)
    fluid.Smooth_Initial()

    dx = L/N

    dt = fluid.dx*CFL

    max_wave_speed = 0

    t = 0

    while t < time - 1e-8:



        Fluid_Time_Advance(fluid, time, dt, problem_type)


        t = t + dt

        #print 't is ', t, ' time end is ', time

    qq = fluid.level_set.qq_n


    print("Finish at time ", t, " interface position is ", qq[0])

    print('Max wave speed is ', max_wave_speed)

    return fluid
